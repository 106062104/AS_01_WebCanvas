# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here
這次作業中許多部份皆是從網路上習得。

1. Brush
beginPath()開始路徑
lineto()讓筆刷可以跟著游標走
stroke()畫在canvas上

2. Eraser
globalCompositeOperation 筆刷的特性
'destination-out' 使橡皮擦可以蓋過筆刷

3. Simple Menu
使用 'range' 改變筆刷及橡皮擦的寬度
做出兩個變數，控制筆刷及橡皮擦

4. Color selector
使用的是html5內建的 input type
type="color"

5. Reset
clearRect() 清除掉整個畫布上的東西
將每個變數的值調回預設值

6. Rect
clearRect() 清除長方形中的線條，使其不重複
strokeRect() 在畫布上畫出長方形

7. Download
toDataURL() 將整張畫布存在變數中

8. Upload
Filereader 讀取檔案
drawimage 將讀取到的圖片畫在畫布上


