function draw() {
    var canvas = document.getElementById('mycanvas');
    var ctx = canvas.getContext('2d');


    //var x = 0;
    //var y = 0;
    var lastx;
    var lasty;
    var tooltype = 'draw';

   /*//undo redo
    var PushArray = new Array();
    var Step = -1;
    Push = function() {
        Step++;
        if (Step < PushArray.length) { PushArray.length = Step; }
        PushArray.push(canvas.toDataURL());
    }


    undo = function() {
        if (Step > 0) {
            Step--;
            var canvasPic = new Image();
            canvasPic.src = PushArray[Step];
            canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
        }
    }


    redo = function() {
        if (Step < PushArray.length - 1) {
            Step++;
            var canvasPic = new Image();
            canvasPic.src = PushArray[Step];
            canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
        }
    }*/

    //change line width
    var slider_line = document.getElementById("linewidth");
    var width_line = 5;

    slider_line.oninput = function () {
        width_line = this.value;
    }

    //change earser width
    var slider_eraser = document.getElementById("erasewidth");
    var width_eraser = 5;

    slider_eraser.oninput = function () {
        width_eraser = this.value;
    }

    //color selector
    var color_sel = document.getElementById("color_selector");
    var color_line;

    color_sel.oninput = function () {
        color_line = this.value;
    }


    function getMousePos(canvas, evt) {
        var rect = canvas.getBoundingClientRect();
        return {
            x: evt.clientX - rect.left,
            y: evt.clientY - rect.top
        };
    }

    function mouseMove(evt) {
        var mousePos = getMousePos(canvas, evt);

        if (tooltype == 'draw') {
            ctx.globalCompositeOperation = 'source-over';
            ctx.lineWidth = width_line;
            ctx.strokeStyle = color_line;
            ctx.lineJoin = "round";
            ctx.lineTo(mousePos.x, mousePos.y);


            ctx.stroke();
            //Push();

        }
        else if (tooltype == 'erase') {
            ctx.globalCompositeOperation = 'destination-out';
            ctx.lineWidth = width_eraser;
            ctx.lineTo(mousePos.x, mousePos.y);

            ctx.stroke();
            //Push();
        }
        else if (tooltype == 'rect') {

            ctx.globalCompositeOperation = 'source-over';
            var width = mousePos.x - lastx;
            var height = mousePos.y - lasty;
            ctx.clearRect(lastx, lasty, width, height);
            ctx.lineWidth = width_line;
            ctx.strokeStyle = color_line;
            ctx.strokeRect(lastx, lasty, width, height);
            //ctx.stroke();
            //Push();

        }

        /*else if (tooltype == 'tri') {
            ctx.globalCompositeOperation = 'source-over';
            ctx.moveTo(lastx, lasty);
            ctx.lineTo(mousePos.x, mousePos.y);
            ctx.lineTo(2 * lastx - mousePos.x, mousePos.y);
            ctx.closePath();
            ctx.stroke();
            //Push();
        }*/


    }

    canvas.addEventListener('mousedown', function (evt) {
        var mousePos = getMousePos(canvas, evt);
        lastx = mousePos.x;
        lasty = mousePos.y;
        ctx.beginPath();
        ctx.moveTo(mousePos.x, mousePos.y);
        //evt.preventDefault();
        canvas.addEventListener('mousemove', mouseMove, false);
    });

    canvas.addEventListener('mouseup', function () {
        canvas.removeEventListener('mousemove', mouseMove, false);
    }, false);

    //decide what kind of tool
    use_tool = function (tool) {
        tooltype = tool;
    }

    //reset
    reset = function () {
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        slider_eraser.value = 5;
        width_eraser = slider_eraser.value;
        slider_line.value = 5;
        width_line = slider_line.value;
        color_sel.value = "black";
        color_line = color_sel.value;
        Push();
    }

    download = function () {
        var image = canvas.toDataURL();

        var tmpLink = document.createElement('a');
        tmpLink.download = 'Image.png';
        tmpLink.href = image;

        document.body.appendChild(tmpLink);
        tmpLink.click();
        document.body.removeChild(tmpLink);
    }

    //upload fileee
    var uploader = document.getElementById('upload');
    uploader.addEventListener('change', upload, false);

    function upload(evt) {
        var reader = new FileReader();
        reader.onload = function (event) {
            var img = new Image();
            img.onload = function () {
                //canvas.width = img.width;
                //canvas.height = img.height;
                ctx.drawImage(img, 0, 0);
            }
            img.src = event.target.result;
        }
        reader.readAsDataURL(evt.target.files[0]);
        //Push();
    }

    
}

window.onload = function () {
    draw();
}//show on window


